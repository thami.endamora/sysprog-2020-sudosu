#!/bin/bash
echo 
echo '==========================================='
echo '          ALSA Sound Device Driver         '
echo '==========================================='

PS3='Do you want to start recording sound? '
options=("YES" "NO (Exit Script)")

select opt in "${options[@]}"
do
	case $opt in
		"YES")
			read -p 'File name (.wav):' filename
			read -p 'Duration in second :' duration
			arecord -d $duration --format=dat $filename
			aplay $filename
			echo "Done"
			;;
		"NO (Exit Script)")
			echo "Bye Bye"
			read -p 'Press [Enter] key to continue...'
			;;
		*) echo invalid option;;
	esac
	exit
done
