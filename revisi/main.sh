#!/bin/bash

echo '==========================================='
echo '         SUDOSU - Ilma Shella Thami        '
echo '==========================================='

PS3='Do you want to automatically start ALSA Sound Device Driver? '
options=("YES" "NO (Exit Script)")
select opt in "${options[@]}"
do
	case $opt in
		"YES")
			bash sudosu-sound.sh
			;;
		"NO (Exit Script)")
			echo 'Bye Bye'
			read -p 'Press [Enter] key to continue...'
			;;
		*) echo invalid option;;
	esac
	exit
done

